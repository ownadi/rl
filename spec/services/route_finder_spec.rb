describe RouteFinder do
  let(:service) { described_class.new }

  describe '#call' do
    it 'validates args' do
      expect { service.call('foo', 'bar', 'baz') }
        .to raise_error(/argument 1|route_role/)
      expect { service.call(RoutePoint.point_types["start_point"], 0, 'foo') }
        .to raise_error(/argument 2|start_city_name/)
      expect { service.call(RoutePoint.point_types["start_point"], 'foo', 0) }
        .to raise_error(/argument 3|finish_city_name/)
    end

    it 'searches matching routes' do
      start_city, finish_city, random_city = (1..3).map { create(:city) }

      #invalid - wrong route_role
      route1 = create(:route, :for_driver)
      create(:route_point, :start, city: start_city, route: route1)
      create(:route_point, city: random_city, route: route1)
      create(:route_point, :finish, city: finish_city, route: route1)

      # valid - starts in start point, ends in finish point
      route2 = create(:route, :for_passenger)
      create(:route_point, :start, city: start_city, route: route2)
      create(:route_point, city: random_city, route: route2)
      create(:route_point, :finish, city: finish_city, route: route2)

      # valid - starts in intermediate point, ends in finish point
      route3 = create(:route, :for_passenger)
      create(:route_point, :start, city: random_city, route: route3)
      create(:route_point, city: start_city, route: route3)
      create(:route_point, :finish, city: finish_city, route: route3)

      # valid - starts in start point, ends in intermediate point
      route4 = create(:route, :for_passenger)
      create(:route_point, :start, city: start_city, route: route4)
      create(:route_point, city: finish_city, route: route4)
      create(:route_point, :finish, city: random_city, route: route4)

      # invalid - route with no points
      create(:route, :for_passenger)

      # invaid - both start and finish points as intermediate ones
      route5 = create(:route, :for_passenger)
      create(:route_point, :start, route: route5)
      create(:route_point, city: start_city, route: route5)
      create(:route_point, city: finish_city, route: route5)
      create(:route_point, :finish, city: random_city, route: route5)

      result = service.call(Route.route_roles['passenger'], start_city.name, finish_city.name)
      expect(result).to include(route2, route3, route4)
      expect(result.count).to eq(3)
    end
  end
end
