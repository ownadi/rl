RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods

  config.before(:suite) { FactoryBot.find_definitions }
end
