require 'rails_helper'

describe Route, type: :model do
  it { should validate_length_of(:description).is_at_most(100) }

  it do
    should define_enum_for(:route_role).
      with_values(
        driver: 0,
        passenger: 1
      )
  end

  it { should validate_inclusion_of(:seats).in_range(1..4) }

  it { should have_db_column(:seats).with_options(default: 1) }
end
