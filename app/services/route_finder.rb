class RouteFinder
  include Contracts::Core
  include Contracts::Builtin

  Contract KeywordArgs[
    route: Optional[RespondTo[:joins, :where]],
    route_point: Optional[RespondTo[:point_types]]
  ] => Any
  def initialize(route: Route, route_point: RoutePoint)
    @route = route
    @route_point = route_point
  end

  Contract Integer, String, String => RespondTo[:where]
  def call(route_role, start_city_name, finish_city_name)
    types = @route_point.point_types

    @route
    .joins("INNER JOIN route_points start_route_points
            ON start_route_points.route_id = routes.id
            AND start_route_points.point_type
            IN (#{types["start_point"]}, #{types["intermediate"]})")
    .joins("INNER JOIN cities start_cities
            ON start_cities.id = start_route_points.city_id")
    .joins("INNER JOIN route_points finish_route_points
            ON finish_route_points.route_id = routes.id
            AND finish_route_points.point_type
            IN (#{types["finish_point"]},
            (CASE WHEN start_route_points.point_type = #{types["start_point"]} THEN #{types["intermediate"]} END))")
    .joins("INNER JOIN cities finish_cities
            ON finish_cities.id = finish_route_points.city_id")
    .where(
      route_role: route_role,
      start_cities: { name: start_city_name }, finish_cities: { name: finish_city_name }
    )
  end
end
