class City < ApplicationRecord
  validates :name, length: { maximum: 50 }

  has_many :route_points
  has_many :routes, through: :route_points
end
