# Recruitment task 19-Q1-R1

## Initial setup

```bash
git clone git@bitbucket.org:ownadi/rl.git
cd rl
bundle
rails db:create db:migrate
```

## Run specs

```bash
rspec
```

In case you'd prefer to see specs in documentation-like format, run the following:

```bash
rspec --format=documentation
```

## Services

### RouteFinder

`RouteFinder` searches and returns an Active Record scope of routes that meet one of the following conditions:

* The route begins in ​`start_city​_name` and finishes in ​`finish_city_name`
* The route goes through intermediate point in `​start_city​_name` and finishes in `finish_city_name`
* The route starts in ​`start_city​_name` and goes through intermediate point in​ `finish_city_name`

#### Usage
The service responds to `#call` method that accepts 3 arguments: `route_role` *(Integer)*
, `start_city_name` *(String)* and `finish_city_name` *(String)*.

```ruby
route_role = Route.route_roles['driver']
results = RouteFinder.new.call(route_role, 'Start city', 'Finish city')
```

You can bring your own dependencies and inject them via constructor:

```ruby
mock = OpenStruct.new
mock.point_types = { "start_point" => 10, "intermediate" => 20, "finish_point" => 30 }
service = RouteFinder.new(route_point: mock)
```
