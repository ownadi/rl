FactoryBot.define do
  factory :route_point do
    route
    city
    point_type { 'intermediate'}
  end

  trait :start do
    point_type { 'start_point' }
  end

  trait :finish do
    point_type { 'finish_point' }
  end
end
