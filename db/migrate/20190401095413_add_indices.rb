class AddIndices < ActiveRecord::Migration[5.2]
  def change
    add_index :cities, :name
    add_index :routes, :route_role
    add_index :route_points, :point_type
  end
end
