class CreateRoutePoints < ActiveRecord::Migration[5.2]
  def change
    create_table :route_points do |t|
      t.belongs_to :city
      t.belongs_to :route
      t.integer :point_type, null: false # I'd name this column `kind` since `type` is a reserved word.
      t.datetime :point_at

      t.timestamps
    end
  end
end
