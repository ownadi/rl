class CreateRoutes < ActiveRecord::Migration[5.2]
  def change
    create_table :routes do |t|
      t.string :description
      t.integer :route_role # I'd name this column just `role`
      t.integer :seats, default: 1

      t.timestamps
    end
  end
end
