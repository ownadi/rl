class RoutePoint < ApplicationRecord
  belongs_to :city
  belongs_to :route

  validates :point_type,
    uniqueness: { scope: :route_id, if: :should_validate_point_type?,
                  message: :duplicated_type }

  enum point_type: { start_point: 0, intermediate: 1, finish_point: 2 }

  private

  def should_validate_point_type?
    single_types = RoutePoint.point_types.except('intermediate').keys
    point_type.in?(single_types)
  end
end
