FactoryBot.define do
  factory :route do
    trait :for_driver do
      route_role { "driver" }
    end

    trait :for_passenger do
      route_role { "passenger" }
    end
  end
end
