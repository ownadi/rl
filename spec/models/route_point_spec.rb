require 'rails_helper'

RSpec.describe RoutePoint, type: :model do
  it { should belong_to(:route) }

  it { should belong_to(:city) }

  it do
    should define_enum_for(:point_type).
      with_values(
        start_point: 0,
        intermediate: 1,
        finish_point: 2
      )
  end

  it 'allows to have only one start point in route' do
    route = create(:route)
    create(:route_point, :start, route: route)
    route_point = build(:route_point, :start, route: route)

    expect(route_point).to be_invalid
    expect(route_point.errors[:point_type]).to be_present
  end

  it 'allows to have only one finish point in route' do
    route = create(:route)
    create(:route_point, :finish, route: route)
    route_point = build(:route_point, :finish, route: route)

    expect(route_point).to be_invalid
    expect(route_point.errors[:point_type]).to be_present
  end

  it 'allows to have more than one intermediate point in route' do
    route = create(:route)
    create(:route_point, route: route)
    route_point = build(:route_point, route: route)

    expect(route_point).to be_valid
  end
end
