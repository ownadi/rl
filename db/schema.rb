# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_01_095413) do

  create_table "cities", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_cities_on_name"
  end

  create_table "route_points", force: :cascade do |t|
    t.integer "city_id"
    t.integer "route_id"
    t.integer "point_type", null: false
    t.datetime "point_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["city_id"], name: "index_route_points_on_city_id"
    t.index ["point_type"], name: "index_route_points_on_point_type"
    t.index ["route_id"], name: "index_route_points_on_route_id"
  end

  create_table "routes", force: :cascade do |t|
    t.string "description"
    t.integer "route_role"
    t.integer "seats", default: 1
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["route_role"], name: "index_routes_on_route_role"
  end

end
