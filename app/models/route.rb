class Route < ApplicationRecord
  validates :description, length: { maximum: 100 }
  validates :seats, inclusion: 1..4

  enum route_role: { driver: 0, passenger: 1 }

  has_many :route_points
  has_many :cities, through: :route_points
end
