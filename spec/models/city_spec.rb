require 'rails_helper'

describe City, type: :model do
  it { should validate_length_of(:name).is_at_most(50) }
end
